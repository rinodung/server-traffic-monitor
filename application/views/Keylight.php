<!DOCTYPE html>
<!-- saved from url=(0033)http://lab.hakim.se/keylight/03/# -->
<html lang="en"><head><meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
		

		<meta name="description" content="The experiments and interactive playground of Hakim El Hattab.">
		<meta name="keywords" content="hakim, elhattab, hakim el hattab, hakim elhattab, interactive, developer, flash, html5, canvas, web, experiments">
		<meta name="author" content="Hakim El Hattab">

		<meta http-equiv="X-UA-Compatible" content="chrome=1">
	    
        <title>Keylight - an audio rhythm experiment.</title>
		

		<link href="./assets/styles.css" rel="stylesheet" media="screen">
    </head>
    <body>
		
	
<canvas id="world" width="100%" height="100%" style="position: absolute; top: 0px; float:left;">
	<p class="noCanvas">You need a <a href="http://www.google.com/chrome">modern browser</a> to view this.</p>
</canvas>

<div id="panel" style="position: absolute; top: 0px;left: 40%; ">
			<h2 id="network">Network Monitor Version 1.0</h2>
			<p><b><a id="decreaseSpeed" href="/#">Decrease</a> / <a id="increaseSpeed" href="#">Increase</a> 
			</b> speed (<span id="speedDisplay">3/6</span>) <b><a id="traffic" href="#">Traffic</a></b></p>
			
			<div id="trafficData" style="display:none">
				<textarea type="textarea" cols='100' rows='40' id="log" style="color: #08de07; background: #000"></textarea>
			</div>
</div>
<script type="text/javascript" src="./assets/widgets.js"></script>
<script src="./assets/bower_components/jquery/dist/jquery.min.js"></script>
<script src="./assets/keylight.js"></script>
<script type="javascript">

</script>
</body>
    </html>